package com.example.jenkins.bitbucket.controller;


import com.example.jenkins.bitbucket.common.AnimalIterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonController {

    @GetMapping("/")
    public String getProject(){
        return "Welcome to the Jenkins Testing Application";
    }

    //----------------------------------First Approach---using naming--------------------------------------------------
    @Autowired
    private AnimalIterface cat;
    @Autowired
    private AnimalIterface dog;

    @GetMapping("getCat")
    public String getCat(){
        return cat.Charachteristics();
    }

    @GetMapping("getDog")
    public String getDog(){
        return dog.Charachteristics();
    }

//-----------------------------------Second Approach---using Qualifier--------------------------------------------------
    @Qualifier(value="elephantService")
    @Autowired
    private AnimalIterface animal1;

    @Qualifier(value="lionService")
    @Autowired
    private AnimalIterface animal2;

    @GetMapping("getElephant")
    public String getElephant(){
        return animal1.Charachteristics();
    }

    @GetMapping("getLion")
    public String getLion(){
        return animal2.Charachteristics();
    }

}
